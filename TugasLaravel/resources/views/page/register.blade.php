@extends('layout.master')
@section('content')
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf 
        <label>Nama Depan</label><br>
        <input type="text" name="fname"> <br><br> 

        <label>Nama Belakang</label> <br>
        <input type="text" name="lname" > <br><br>

        <label>Gender:</label> <br> <br>
        <input type="radio" name="Gender"> Male <br> 
        <input type="radio" name="Gender"> Female <br> 
        <input type="radio" name="Gender"> Other <br> <br>

        <label>Nationality:</label> <br> <br>

        <select name="Nationality"> 
            <option value="">American</option>
            <option value="">British</option>
            <option value="">Indonesian</option>
            <option value="">Japanese</option>
        </select> <br> <br>

        <label > Language Spoken:</label> <br> <br>
        <input type="checkbox" name="Language Spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken"> English <br>
        <input type="checkbox" name="Language Spoken"> Other <br> <br>

        <label>Bio</label> <br>
        <textarea cols="30" rows="10"></textarea> <br> <br>

        <input type="submit" value="Kirim"> 
@endsection